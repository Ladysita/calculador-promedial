package usuario.movil.notas;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class HelperDB extends SQLiteOpenHelper {


    private static final String TABLE_CONTROL_DATOS =
            "CREATE TABLE  parametros (institucion text, cedula text, nombre text, materia text, nota1 int,  nota2 int, promedio text, dato text)";

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "MiBasedeDatos.db";


    public HelperDB(Context context) {
        super(context, DATABASE_NAME, null,  DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CONTROL_DATOS);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS parametros");
        onCreate(db);

    }


    public String LeerTodo() {

        String consulta = "";
        Cursor cursor = this.getReadableDatabase().rawQuery("SELECT * FROM parametros", null);
        if (cursor.moveToFirst()){
            do{

                String Institucion = cursor.getString(cursor.getColumnIndex("institucion"));
                String Cedula = cursor.getString(cursor.getColumnIndex("cedula"));
                String Nombre = cursor.getString(cursor.getColumnIndex("nombre"));
                String Materia = cursor.getString(cursor.getColumnIndex("materia"));
                String Nota1 = cursor.getString(cursor.getColumnIndex("nota1"));
                String Nota2 = cursor.getString(cursor.getColumnIndex("nota2"));
                String Promedio = cursor.getString(cursor.getColumnIndex("promedio"));
                String Dato = cursor.getString(cursor.getColumnIndex("dato"));


                consulta +=  "Institucion : " + Institucion + "\n" + "Cedula : " + Cedula + "\n" + "Nombre : " + Nombre + "\n" + "Materia : " + Materia + "\n" + "Nota1 : " + Nota1 + "\n"  + "Nota2 : " + Nota2 +  "\n" + "Promedio : " + Promedio  +  "\n" + "Estado : " + Dato  +  "\n"   +  "\n" +  "\n";
            }while (cursor.moveToNext());
        }
        return consulta;
    }


    public void eliminar (){
        SQLiteDatabase db = getReadableDatabase();
        db.delete(TABLE_CONTROL_DATOS, null, null);



    }
}
