package usuario.movil.notas;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ingreso extends Activity {

    EditText cedula, nombre,materia, institucion,  nota1, nota2;
    Button guardar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingreso);

        View decorView = getWindow().getDecorView();
        // Hide both the navigation bar and the status bar.
        // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
        // a general rule, you should design your app to hide the status bar whenever you
        // hide the navigation bar.
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        institucion = (EditText)findViewById(R.id.institucion);
        cedula = (EditText)findViewById(R.id.cedula);
        nombre = (EditText)findViewById(R.id.nombres);
        materia = (EditText)findViewById(R.id.materia);
        nota1 = (EditText)findViewById(R.id.nota1);
        nota2 = (EditText)findViewById(R.id.nota2);
        guardar = (Button)findViewById(R.id.guardar);



        final HelperDB helperbd = new HelperDB(getApplicationContext());




        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = institucion.getText().toString();
                if (TextUtils.isEmpty(name)){
                    institucion.setError("Complete el campo");
                    institucion.requestFocus();
                    return;
                }

                String name2 = cedula.getText().toString();
                if (TextUtils.isEmpty(name2)){
                    cedula.setError("Complete el campo");
                    cedula.requestFocus();
                    return ;
                }
                String name4 = nombre.getText().toString();
                if (TextUtils.isEmpty(name4)){
                    nombre.setError("Complete el campo");
                    nombre.requestFocus();
                    return ;

                }
                String name8 = materia.getText().toString();
                if (TextUtils.isEmpty(name8)){
                    materia.setError("Complete el campo");
                    materia.requestFocus();
                    return ;
                }

                String name1 = nota1.getText().toString();
                if (TextUtils.isEmpty(name1)){
                    nota1.setError("Complete el campo");
                    nota1.requestFocus();
                    return;
                }

                String name3 = nota2.getText().toString();
                if (TextUtils.isEmpty(name3)){
                    nota2.setError("Complete el campo");
                    nota2.requestFocus();
                    return ;
                }





                float not1 = Float.parseFloat(nota1.getText().toString());
                float not2 = Float.parseFloat(nota2.getText().toString());
                int promedio = (int)  ((not1 + not2));
                   if(not1 >= 11 || not2 >= 11){
                    Toast.makeText(getApplicationContext(),  "No se pueden subir los datos, " +
                            "los valores de la nota debe ser maximo 10", Toast.LENGTH_SHORT).show();

                }else if(promedio >= 14 && promedio <=20 ){

                    String dat = ("Aprobado");
                    SQLiteDatabase db = helperbd.getWritableDatabase();
                    ContentValues valores = new ContentValues();
                    valores.put("institucion",institucion.getText().toString());
                    valores.put("cedula",cedula.getText().toString());
                    valores.put("nombre", nombre.getText().toString());
                    valores.put("materia", materia.getText().toString());
                    valores.put("nota1", nota1.getText().toString());
                    valores.put("nota2", nota2.getText().toString());
                    valores.put("promedio", promedio);
                    valores.put("dato", dat);


                    Long IdGuardado = db.insert("parametros", "cedula",  valores);
                    Toast.makeText(getApplicationContext(),
                            "Registro guardado: ",
                            Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(ingreso.this, inicial.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);

                }else if(promedio >= 0 && promedio <14){

                    String dat = ("Reprobado");
                    SQLiteDatabase db = helperbd.getWritableDatabase();
                    ContentValues valores = new ContentValues();
                    valores.put("institucion",institucion.getText().toString());
                    valores.put("cedula",cedula.getText().toString());
                    valores.put("nombre", nombre.getText().toString());
                    valores.put("materia", materia.getText().toString());
                    valores.put("nota1", nota1.getText().toString());
                    valores.put("nota2", nota2.getText().toString());
                    valores.put("promedio", promedio);
                    valores.put("dato", dat);


                    Long IdGuardado = db.insert("parametros", "cedula",  valores);
                    Toast.makeText(getApplicationContext(),
                            "Registro guardado: ",
                            Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(ingreso.this, inicial.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);



                }
                else if(not1 >= 11 && not2 >= 11){
                    Toast.makeText(getApplicationContext(),  "No se pueden subir los datos, " +
                            "los valores de la nota debe ser maximo 10", Toast.LENGTH_LONG).show();
            }
            }
        });
    }
}
