package usuario.movil.notas;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class login extends Activity implements View.OnClickListener {
    private EditText usuario;
    private EditText contraseña;

    private Button Login;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        View decorView = getWindow().getDecorView();
        // Hide both the navigation bar and the status bar.
        // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
        // a general rule, you should design your app to hide the status bar whenever you
        // hide the navigation bar.
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);





        usuario = (EditText)findViewById(R.id.TXTUser);
        contraseña = (EditText)findViewById(R.id.TXTPass);

        Login = (Button)findViewById(R.id.BTNIniciarSesion);



        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate(usuario.getText().toString(), contraseña.getText().toString());

            }
        });
    }

    private void validate(String userName, String userPassword){
        if((userName.equals("estudiante")) && (userPassword.equals("123456789"))){
            Intent intent = new Intent(login.this, inicial.class);
            startActivity(intent);
        }else{
            Toast.makeText(login.this, "datos incorrectos, intentelo nuevamente",Toast.LENGTH_SHORT).show();;

        }


    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    public void onClick(View view) {

    }
}
