package usuario.movil.notas;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class inicial extends Activity {

    Button ingreso, eliminar, consulta, salir;
    Context context;
    HelperDB helperDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicial);

        View decorView = getWindow().getDecorView();
        // Hide both the navigation bar and the status bar.
        // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
        // a general rule, you should design your app to hide the status bar whenever you
        // hide the navigation bar.
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);


        ingreso= (Button)findViewById(R.id.ingresar);
        ingreso.setOnClickListener(new View.OnClickListener() {
            @Override


            public void onClick(View v) {
                Intent registro = new Intent(inicial.this, ingreso.class);
                startActivity(registro);

            }
        });
        consulta= (Button)findViewById(R.id.consultar);
        consulta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mostrar = new Intent(inicial.this, datos.class);
                startActivity(mostrar);

            }
        });

        salir= (Button)findViewById(R.id.salir);
        salir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent salir = new Intent(inicial.this, login.class);
                startActivity(salir);

            }
        });
        
        eliminar = (Button) findViewById(R.id.borrar);
        eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final HelperDB helperbd = new HelperDB(getApplicationContext());
                helperbd.eliminar();
            }
        });

    }
    @Override
    public void onBackPressed (){
        moveTaskToBack(true);
    }

}